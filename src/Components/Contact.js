import React, { Component } from 'react';

class Contact extends Component {
  render() {

    if(this.props.data){
      var email = this.props.data.email;
      var message = this.props.data.contactmessage;
    }

    return (
      <section id="contact">
            <div className="row">
               <div className="columns contact-details">
               </div>
            </div>

         <div className="row section-head">

            <div className="two columns header-col">

               <h1><span>Get In Touch.</span></h1>

            </div>

            <div className="ten columns">

                  <p className="lead">{message}</p>
                  <h2>Email address</h2>
                  <p className="email"><span>{email}</span></p>

            </div>

         </div>

   </section>
    );
  }
}

export default Contact;
