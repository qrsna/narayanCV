#!/bin/bash
echo "Running build"
npm run build
if [ $? == 1 ]; then
  echo "Error with npm build"
  exit 1;
fi

echo "Removing s3 content"
aws --profile qrsna s3 rm s3://narayan.pro/ --recursive
if [ $? == 1 ]; then
  echo "Error with removing s3 content"
  exit 1;
fi

echo "Deploying new site to s3"
aws --profile qrsna s3 cp build/ s3://narayan.pro/ --recursive

if [ $? == 1 ] ;then
  echo "Error with deplyong site"
  exit 1;
fi
